from django.contrib.auth.models import User
from django.test import TestCase, Client


class AdminTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_superuser_right_credentials(self):
        """test case to test superuser with right credentials"""

        my_admin = User(username='nikita', email='nikita@email.com')
        my_admin.set_password('nikita')
        my_admin.is_superuser = True
        my_admin.save()
        response = self.client.get('/admin/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('Log in' in response.content.decode('utf-8'))
        login_response = self.client.login(username='nikita', email='nikita@email.com',
                                           password='nikita')
        self.assertTrue(login_response)

    def test_superuser_wrong_credentials(self):
        """test case to test superuser with wrong credentials"""

        my_admin = User(username='nikita', email='nikita@email.com')
        my_admin.set_password('nikita')
        my_admin.is_superuser = True
        my_admin.save()
        response = self.client.get('/admin/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('Log in' in response.content.decode('utf-8'))
        login_response = self.client.login(username='nikita', email='nikita@email.com',
                                           password='password')
        self.assertTrue(login_response)

    def test_staff_user_right_credentials(self):
        """test case to test staff user with right credentials"""

        my_admin = User(username='nikita', email='nikita@email.com')
        my_admin.set_password('nikita')
        my_admin.is_staff = True
        my_admin.save()
        response = self.client.get('/admin/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('Log in' in response.content.decode('utf-8'))
        login_response = self.client.login(username='nikita', email='nikita@email.com',
                                           password='nikita')
        self.assertTrue(login_response)

    def test_staff_user_wrong_credentials(self):
        """test case to test staff user with wrong credentials"""

        my_admin = User(username='nikita', email='nikita@email.com')
        my_admin.set_password('nikita')
        my_admin.is_staff = True
        my_admin.save()
        response = self.client.get('/admin/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('Log in' in response.content.decode('utf-8'))
        login_response = self.client.login(username='nikita', email='nikita@email.com',
                                           password='password')
        self.assertTrue(login_response)

    def test_non_staff_user_right_credentials(self):
        """test case to test non staff user with right credentials"""

        my_admin = User(username='nikita', email='nikita@email.com')
        my_admin.set_password('nikita')
        my_admin.is_staff = False
        my_admin.save()
        response = self.client.get('/admin/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('Log in' in response.content.decode('utf-8'))
        login_response = self.client.login(username='nikita', email='nikita@email.com',
                                           password='nikita')
        self.assertTrue(login_response)
