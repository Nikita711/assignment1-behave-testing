# Django Admin authentication functional test cases

>## Tehcnologies used
>* Python 3.5.10
>* Django 
>## Helper library for BDD
>* Behave
>* Selenium for browser based testing
---
## Run the code
>### Setup your own virtual environment
>* Run ```python3 -m venv venv```
>* Run ```source venv/bin/activate```
>* Run ```pip install -r requirements.txt ```
---

>### Run the unit test
>* ```python manage.py test```

---

>### Run the test runner based test cases
>* ```python manage.py behave features/main.features```

---

>### Run the browser based test cases using selenium

>* [click here download latest executable geckodriver from here to run latest firefox using selenium]('https://github.com/mozilla/geckodriver/releases')

>* ```export PATH=$PATH:/path/to/directory/of/executable/downloaded/in/previous/step```
>* run ```python manage.py runserver```

>* run ```python manage.py behave features/browser.features```

---
