from selenium import webdriver


def before_all(context):
    context.browser = webdriver.Firefox()
    context.root_url = "http://127.0.0.1:8000"


def after_scenario(context, feature):
    context.browser.get(context.root_url + '/admin/logout/')


def after_feature(context, feature):
    context.browser.close()


def after_all(context):
    context.browser.quit()
