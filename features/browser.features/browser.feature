Feature: Admin Authentication


  Scenario: Successful staff user Login
    Given Login page is loaded
    And "Staff" User registers "achintya" as username and "achintya" as password
    And User enters "achintya" as username and "achintya" as password
    When User clicks submit
    Then "WELCOME" should be displayed

  Scenario: Successful superuser Login
    Given Login page is loaded
    And "Super" User registers "ranjan" as username and "ranjan" as password
    And User enters "ranjan" as username and "ranjan" as password
    When User clicks submit
    Then "WELCOME" should be displayed

  Scenario: Successful non staff user Login
    Given Login page is loaded
    And "Non staff" User registers "temppp" as username and "temppp" as password
    And User enters "temppp" as username and "temppp" as password
    When User clicks submit
    Then "Please enter the correct username and password" is displayed

  Scenario: Unsuccessful staff user Login
    Given Login page is loaded
    And "Staff" User registers "temp" as username and "temp" as password
    And User enters "temp" as username and "temporary" as password
    When User clicks submit
    Then "Please enter the correct username and password" is displayed


  Scenario: Unsuccessful staff user Login
    Given Login page is loaded
    And "Staff" User registers "tempp" as username and "tempp" as password
    And User enters "tempp" as username and "temporary" as password
    When User clicks submit
    Then "Please enter the correct username and password" is displayed