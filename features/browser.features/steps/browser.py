from behave import *
from django.contrib.auth.models import User
from selenium.common.exceptions import NoSuchElementException

from features.utils.constants import error


@given('Login page is loaded')
def load_login_page(context):
    """to load login page"""

    context.browser.get(context.root_url + '/admin/')


@given('"{status}" User registers "{username}" as username and "{password}" as password')
def create_user(context, status, username, password):
    """to create and save the user"""

    my_admin = User(username=username)
    my_admin.set_password(password)
    if status == "Super":
        my_admin.is_superuser = True
        my_admin.is_staff = True
    elif status == "Staff":
        my_admin.is_staff = True
    elif status == "Non staff":
        my_admin.is_staff = False
    my_admin.is_active = True
    my_admin.save()


@given('User enters "{username}" as username and "{password}" as password')
def try_login(context, username, password):
    """function to login"""

    try:
        uname_elem = context.browser.find_element_by_id('id_username')
        passwd_elem = context.browser.find_element_by_id('id_password')
        uname_elem.send_keys(username)
        passwd_elem.send_keys(password)
    except NoSuchElementException:
        assert False, error


@when(u'User clicks submit')
def click_submit(context):
    """function to click on login button"""

    try:
        submit_elem = context.browser.find_element_by_xpath('//input[@value="Log in"]')
        submit_elem.click()
    except NoSuchElementException:
        assert False, error


@then(u'"{msg}" should be displayed')
def check_message_on_valid_login(context, msg):
    """to validate successful login"""

    try:
        msg_elem = context.browser.find_element_by_id('user-tools')
        assert msg in msg_elem.text

    except NoSuchElementException:
        assert False, error


@then(u'"{msg}" is displayed')
def check_message_on_invalid_login(context, msg):
    """to validate unsuccessful login"""

    try:
        msg_elem = context.browser.find_elements_by_class_name("errornote")
        assert msg in msg_elem[0].text

    except NoSuchElementException:
        assert False, error
