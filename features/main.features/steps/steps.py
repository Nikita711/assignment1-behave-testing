from behave import *
from django.contrib.auth.models import User
from django.test import Client


@given('"{status}" User registers "{username}" as username and "{password}" as password')
def create_user(context, status, username, password):
    """to create and save the user"""
    my_admin = User(username=username)
    my_admin.set_password(password)
    if status == "Super":
        my_admin.is_superuser = True
        my_admin.is_staff = True
    elif status == "Staff":
        my_admin.is_staff = True
    elif status == "Non staff":
        my_admin.is_staff = False
    my_admin.is_active = True
    my_admin.save()


@when('User enters "{username}" as username and "{password}" as password')
def try_login(context, username, password):
    """to test when super user tries to login"""

    context.client = Client()
    context.response = context.client.login(username=username,
                                            password=password)


@then(u'"{msg}" should be displayed')
def step_impl_on_valid_login(context, msg):
    """to validate login"""

    assert "True" in str(context.response)


@then(u'"{msg}" is displayed')
def step_imp_on_invalid_login(context, msg):
    """to validate login with wrong credentials"""

    assert "False" in str(context.response)
