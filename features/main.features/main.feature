Feature: Test authentication part against django admin for super user, staff user, non staff user both with
  valid and invalid credentials

  Scenario: Successful superuser Login
    Given "Super" User registers "nikita" as username and "nikita" as password
    When  User enters "nikita" as username and "nikita" as password
    Then "Welcome, nikita" should be displayed

  Scenario: Unsuccessful superuser Login
    Given "Super" User registers "ranjan" as username and "ranjan" as password
    When  User enters "ranjan" as username and "password" as password
    Then "Please enter the correct username and password for a staff account" is displayed

  Scenario: Successful staff user Login
    Given "Staff" User registers "temp" as username and "temp" as password
    When  User enters "temp" as username and "temp" as password
    Then "Welcome, nikita" should be displayed

  Scenario: Unsuccessful staff user Login
    Given "Staff" User registers "tempp" as username and "tempp" as password
    When  User enters "tempp" as username and "temp" as password
    Then "Please enter the correct username and password for a staff account" is displayed

  Scenario: Successful non staff user Login
    Given "Non staff" User registers "default" as username and "default" as password
    When  User enters "default" as username and "default" as password
    Then "Welcome, default" should be displayed

